Dependencies
============

## Django Mandrill adapter

Package: **djrill==0.4.0**

Repository: [https://github.com/brack3t/Djrill](https://github.com/brack3t/Djrill)

Install: 

	pip install djrill==0.4.0
	
Description: Djrill is an email backend and new message class for Django users that want to take advantage of the Mandrill transactional email service from MailChimp.

Implementation: Handle sending by processing the email data and passing to the Mandrill API.

## CSS utils (optional is using local inlining rather than Mandrill css inlining which is recommended and set by default)

Package: **cssutils==0.9.10**

Repository: [https://bitbucket.org/cthedot/cssutils/](https://bitbucket.org/cthedot/cssutils/)

Install: 

	pip install cssutils==0.9.10

Description: A CSS Cascading Style Sheets library for Python

Implemention: Insert CSS from the header into inline CSS, for global email client supports. Many email clients will remove the header style so it should entered inline to ensure proper rendering. It also ease the style management by only editing the header style rather than inputting inline CSS directly.