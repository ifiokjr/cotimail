Usage
=====

Create a custom notice
----------------------

Notices are basically an email type where the context and the template may vary. Also, it can be configured with any attributes that Mandrill allows to be passed via the API. A list of the attributes can be find on the [Djrill documentation](https://djrill.readthedocs.org/en/master/usage/sending_mail.html#mandrill-specific-options).

The notice will inherit the BaseNotice class and must follow those guidelines:

- Its class name must always terminate with 'Notice'.
- It must have a 'name' attribute
- It must have an  'identifier' attribute


	from cotimail.notice import Notice

	class CustomNotice(Notice):
		# Use as a list display
		name = 'Custom name' 
		# Use for the preview URL as a slug, so it must not contains spaces or other symbols than lowercase letters and hyphens
		identifier = 'custom-name' 
		# Defines an HTML template for this notice
		html_template = 'path/to/template.html'
		text_template = 'path/to/template.txt'

		# A context passed to every request (merge with `context`)
		default_context = {'site_url':'http://example.com'}
		
		# A JSON representation of the context dictionary, which is the format it will be saved as in the EmailLog
		context = {'subject_var': 'My subject variable'}
		
		# Passing on come context variables to build the subject line 
		subject = u'%s %s' % ('Your enquiry for', context['subject_var'])
	
> It is recommend to add your notice template inside the app `templates` folder to keep it organised.

Send a notice
-------------

	# Initiate the notice with necessary variables
	notice = HelloNotice(
		sender = '%s <%s>' % ('Guillaume Piot', 'guillaume@cotidia.com'),
		# A list of recipients emails
		recipients = ['guillaume@piot.co.uk'],
		context = {'sender_name':'Guillaume Piot'},
		
		# Optional: hook to an object
		content_object = obj
	)
	# Send the notice straight away
	notice.send()

Attachments
-----------

Attachments must be added as an attribute of the notice object:

	notice.attachments = [{"content_type": "application/pdf", "file_path": "/file/doc.pdf" }]

When the notice is processed to be sent, it will attach its file to the message as such:

	if self.attachments:
		for attachment in self.attachments:
			msg.attach_file(attachment['file_path'], attachment['content_type'])

